<?php

use Illuminate\Support\Facades\Route;

Route::get('/index', function () {
    return view('index');
});

Route::get('/', function () {
    return view('accueil');
});

Route::get('/services', function () {
    return view('services');
});

Route::get('/habitats', function () {
    return view('habitats');
});

Route::get('/avis', function () {
    return view('avis');
});

Route::get('/contact', function () {
    return view('contact');
});

Route::get('/connexion', [App\Http\Controllers\ConnexionController::class, 'formulaire']);
Route::post('/connexion', [App\Http\Controllers\ConnexionController::class, 'traitement']);


Route::get('/savane', function () {
    return view('savane');
});

Route::get('/jungle', function () {
    return view('jungle');
});

Route::get('/marais', function () {
    return view('marais');
});

Route::get('/mon-compte', [App\Http\Controllers\CompteController::class, 'accueil']);

Route::get('/deconnexion', [App\Http\Controllers\CompteController::class, 'deconnexion']);

Route::post('/logout', function () {
    auth()->logout();
    return redirect('/');
})->name('logout');


Route::middleware(['isAdmin'])->group(function () {
    Route::get('admin', [\App\Http\Controllers\AnimalController::class, 'indexDashboardAnimal']);
    Route::get('admin', [\App\Http\Controllers\AnimalController::class, 'indexDashboardAnimal'])->name('admin.dashboard');

    Route::get('gestionDesServices', [\App\Http\Controllers\ServiceController::class, 'indexGestionService'])->name('gestionDesServices');
    Route::resource('gestionDesServices', \App\Http\Controllers\ServiceController::class)->only(['destroy', 'show', 'update', 'create', 'store']);

    Route::get('gestionDesHabitats', [\App\Http\Controllers\HabitatController::class, 'indexGestionHabitat'])->name('gestionDesHabitats');
    Route::resource('gestionDesHabitats', \App\Http\Controllers\HabitatController::class)->only(['destroy', 'show', 'update', 'create', 'store']);

    Route::get('gestionDesAnimaux', [\App\Http\Controllers\AnimalController::class, 'indexGestionAnimal'])->name('gestionDesAnimaux');
    Route::resource('gestionDesAnimaux', \App\Http\Controllers\AnimalController::class)->only(['destroy', 'show', 'edit', 'update', 'create', 'store']);

    Route::get('gestionDesRaces', [\App\Http\Controllers\RaceController::class, 'indexGestionRace'])->name('gestionDesRaces');
    Route::resource('gestionDesRaces', \App\Http\Controllers\RaceController::class)->only(['destroy', 'show', 'update', 'create', 'store']);

    Route::get('ajout-Compte', [\App\Http\Controllers\UserController::class, 'index'])->name('ajoutCompte');
    Route::resource('ajout-Compte', \App\Http\Controllers\UserController::class)->only(['store']);

    Route::get('/indexHoraires', function () {
        return view('indexHoraires');
    });

    Route::get('/indexComptesRendus', [App\Http\Controllers\VeterinaryReportController::class, 'adminIndex'])->name('admin.veterinaryReports.index');
});

use App\Http\Controllers\AnimalController;
use App\Http\Controllers\EmployeeNourritureController;
use App\Http\Controllers\VeterinaryReportController;
Route::get('/animal/{id}', [AnimalController::class, 'showAnimalDetails'])->name('animal.details');
Route::post('/animal/{id}/increment-view', [AnimalController::class, 'incrementView'])->name('animal.incrementView');

Route::get('/jungle', [AnimalController::class, 'jungle']);
Route::get('/marais', [AnimalController::class, 'marais']);
Route::get('/savane', [AnimalController::class, 'savane']);

Route::middleware(['isVeterinary'])->group(function () {
    Route::get('veterinary', [VeterinaryReportController::class, 'index'])->name('veterinaryReports.index');
    Route::resource('veterinaryReports', VeterinaryReportController::class)->only(['create', 'store', 'show', 'edit', 'update', 'destroy']);

    Route::get('/nourriture', [EmployeeNourritureController::class, 'veterinaryIndex'])->name('veterinary.employee.index');
});

Route::middleware(['isEmployee'])->group(function () {
    Route::get('employee', [EmployeeNourritureController::class, 'index'])->name('employee.index');
    Route::get('employee/create', [EmployeeNourritureController::class, 'create'])->name('employee.create');
    Route::post('employee', [EmployeeNourritureController::class, 'store'])->name('employee.store');
    Route::get('employee/{id}/edit', [EmployeeNourritureController::class, 'edit'])->name('employee.edit');
    Route::put('employee/{id}', [EmployeeNourritureController::class, 'update'])->name('employee.update');
    Route::delete('employee/{id}', [EmployeeNourritureController::class, 'destroy'])->name('employee.destroy');

});