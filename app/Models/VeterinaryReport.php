<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VeterinaryReport extends Model
{
    use HasFactory;
    protected $fillable = ['date', 'animal_id', 'detail'];
    protected $hidden = ['created_at', 'updated_at'];

    public function animal()
    {
        return $this->belongsTo(Animal::class);
    }
}
