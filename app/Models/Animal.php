<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Animal extends Model
{
    use HasFactory;
    protected $fillable = ['firstname', 'status', 'scoreView', 'habitat_id', 'race_id', 'image_path'];
    protected $hidden = ['created_at', 'updated_at'];

    public function veterinaryReports()
    {
        return $this->hasMany(VeterinaryReport::class);
    }
    public function Habitat()
    {
        return $this->belongsTo(Habitat::class);
    }
    public function Race()
    {
        return $this->belongsTo(Race::class);
    }
}