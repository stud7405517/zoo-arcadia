<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Habitat extends Model
{
    use HasFactory;
    protected $fillable = ['name','description','habitatCommentary'];
    protected $hidden = ['created_at','updated_at'];
    
    public function Image()
    {
        return $this->hasMany(Image::class);
    }
    public function Animal()
    {
        return $this->belongsTo(Animal::class);
    }
}
