<?php

namespace App\Http\Controllers;

use App\Models\Habitat;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreHabitatRequest;
use App\Http\Requests\UpdateHabitatRequest;

class HabitatController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    public function indexGestionHabitat()
    {
        $habitats = Habitat::all();
        return view('gestion.indexHabitat',['habitats' => $habitats]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('gestion.createHabitat');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreHabitatRequest $request)
    {
        $request->validate([
            'nomHabitat' => 'required|string|max:255',
            'description' => 'required|string',
        ]);

        $habitat = new Habitat();
        $habitat->name = $request->input('nomHabitat');
        $habitat->description = $request->input('description');
        $habitat->save();

        return redirect()->route('gestionDesHabitats')->with('success', 'Habitat ajouté avec succès.');
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $habitat = Habitat::find($id);
        return view('gestion.showHabitat',['habitat' => $habitat]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Habitat $habitat)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateHabitatRequest $request, $id)
    {
        $habitat = Habitat::find($id);

        $habitat->name = $request->input('nomHabitat');
        $habitat->description = $request->input('description');

        $habitat->save();

        return redirect()->route('gestionDesHabitats')->with('success', 'Les informations de l\'habitat ont été mises à jour avec succès.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $habitat = Habitat::find($id);

        if ($habitat->delete()){
            return response()->json(['message' => 'Success']);
        }else {
            return response()->json(['message' => 'Error']);
        }
    }
}
