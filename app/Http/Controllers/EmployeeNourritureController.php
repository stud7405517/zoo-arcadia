<?php

namespace App\Http\Controllers;

use App\Models\Nourriture;
use App\Models\Animal;
use Illuminate\Http\Request;

class EmployeeNourritureController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $nourritures = Nourriture::with('animal')->get();
        return view('dashboardEmployee.index', compact('nourritures'));
    }

    public function veterinaryIndex(Request $request)
    {
        $animals = Animal::all();

        $query = Nourriture::query();

        if ($request->has('animal_id') && $request->animal_id != '') {
            $query->where('animal_id', $request->animal_id);
        }

        if ($request->has('start_date') && $request->start_date != '') {
            $query->where('date', '>=', $request->start_date);
        }

        if ($request->has('end_date') && $request->end_date != '') {
            $query->where('date', '<=', $request->end_date);
        }

        $nourritures = $query->with('animal')->get();

        return view('dashboardVeterinary.nourriture', compact('nourritures', 'animals'));
    }
    
    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $animals = Animal::all();
        return view('dashboardEmployee.createNourriture', compact('animals'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'date' => 'required|date_format:d/m/Y',
            'heure' => 'required|date_format:H:i',
            'animal_id' => 'required|exists:animals,id',
            'detail' => 'required|string|max:255',
            'quantité' => 'required|string|max:255',
        ]);

        // Convert date to Y-m-d format
        $date = \Carbon\Carbon::createFromFormat('d/m/Y', $request->date)->format('Y-m-d');

        Nourriture::create([
            'date' => $date,
            'heure' => $request->heure,
            'animal_id' => $request->animal_id,
            'detail' => $request->detail,
            'quantité' => $request->quantité,
        ]);

        return redirect()->route('employee.index')->with('success', 'Nourriture ajoutée avec succès');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $nourriture = Nourriture::with('animal')->findOrFail($id);
        $animals = Animal::all();
        return view('dashboardEmployee.editNourriture', ['nourriture' => $nourriture, 'animals' => $animals]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {

        $nourriture = Nourriture::findOrFail($id);
        $nourriture->date = \DateTime::createFromFormat('d/m/Y', $request->date)->format('Y-m-d');
        $nourriture->heure = $request->input('heure');
        $nourriture->animal()->associate($request->input("animal_id"));
        $nourriture->detail = $request->input('detail');
        $nourriture->quantité = $request->input('quantité');
        $nourriture->save();

        return redirect()->route('employee.index')->with('success', 'Nourriture modifiée avec succès');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $nourriture = Nourriture::findOrFail($id);
        $nourriture->delete();
        return redirect()->route('employee.index')->with('success', 'Nourriture supprimée avec succès');
    }
}
