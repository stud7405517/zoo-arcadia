<?php

namespace App\Http\Controllers;

use App\Models\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreServiceRequest;
use App\Http\Requests\UpdateServiceRequest;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        // $services = Service::all();
        // return view('gestion-des-services');
        //dump("test");
    }

    public function indexGestionService()
    {
        $services = Service::all();
        return view('gestion.indexService',['services' => $services]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('gestion.createService');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreServiceRequest $request)
    {
        $request->validate([
            'nomService' => 'required|string|max:255',
            'description' => 'required|string',
        ]);

        $service = new Service();
        $service->name = $request->input('nomService');
        $service->description = $request->input('description');
        $service->save();

        return redirect()->route('gestionDesServices')->with('success', 'Service ajouté avec succès.');
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $service = Service::find($id);
        return view('gestion.showService',['service' => $service]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Service $service)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateServiceRequest $request, $id)
    {
        $service = Service::find($id);

        $service->name = $request->input('nomService');
        $service->description = $request->input('description');

        $service->save();

        return redirect()->route('gestionDesServices')->with('success', 'Les informations du service ont été mises à jour avec succès.');
    }

    
    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $service = Service::find($id);

        if ($service->delete()){
            return response()->json(['message' => 'Success']);
        }else {
            return response()->json(['message' => 'Error']);
        }
    }
}
