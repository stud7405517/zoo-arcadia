<?php

namespace App\Http\Controllers;

use App\Models\Race;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreRaceRequest;
use App\Http\Requests\UpdateRaceRequest;

class RaceController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    public function indexGestionRace()
    {
        $races = Race::all();
        return view('gestion.indexRace',['races' => $races]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('gestion.createRace');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreRaceRequest $request)
    {
        $request->validate([
            'nomRace' => 'required|string|max:255',
        ]);

        $race = new Race();
        $race->name = $request->input('nomRace');
        $race->save();

        return redirect()->route('gestionDesRaces')->with('success', 'Race ajouté avec succès.');
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $race = Race::find($id);
        return view('gestion.showRace',['race' => $race]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Race $race)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRaceRequest $request, $id)
    {
        $race = Race::find($id);

        $race->name = $request->input('nomRace');

        $race->save();

        return redirect()->route('gestionDesRaces')->with('success', 'Les informations de la race ont été mises à jour avec succès.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $race = Race::find($id);

        if ($race->delete()){
            return response()->json(['message' => 'Success']);
        }else {
            return response()->json(['message' => 'Error']);
        }
    }
}
