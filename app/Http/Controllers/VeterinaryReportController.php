<?php

namespace App\Http\Controllers;

use App\Models\VeterinaryReport;
use App\Models\Animal;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreVeterinaryReportRequest;
use App\Http\Requests\UpdateVeterinaryReportRequest;

class VeterinaryReportController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $reports = VeterinaryReport::with('animal')->get();
        return view('dashboardVeterinary.index', ['reports' => $reports]);
    }

    public function adminIndex(Request $request)
    {
        $animals = Animal::all();

        $query = VeterinaryReport::query();

        if ($request->has('animal_id') && $request->animal_id != '') {
            $query->where('animal_id', $request->animal_id);
        }

        if ($request->has('start_date') && $request->start_date != '') {
            $query->where('date', '>=', $request->start_date);
        }

        if ($request->has('end_date') && $request->end_date != '') {
            $query->where('date', '<=', $request->end_date);
        }

        $reports = $query->with('animal')->get();

        return view('dashboardAdmin.indexComptesRendus', compact('reports', 'animals'));
    }
    
    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $animals = Animal::all();
        return view('dashboardVeterinary.createReport', ['animals' => $animals]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'date' => 'required|date_format:d/m/Y',
            'animal_id' => 'required|exists:animals,id',
            'detail' => 'required|string',
        ]);

        $date = \DateTime::createFromFormat('d/m/Y', $request->date)->format('Y-m-d');

        VeterinaryReport::create([
            'date' => $date,
            'animal_id' => $request->animal_id,
            'detail' => $request->detail,
        ]);

        return redirect()->route('veterinaryReports.index')->with('success', 'Compte rendu ajouté avec succès.');
    }
    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $report = VeterinaryReport::with('animal')->findOrFail($id);
        return view('dashboardVeterinary.showReport', ['report' => $report]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $report = VeterinaryReport::with('animal')->findOrFail($id);
        $animals = Animal::all();
        return view('dashboardVeterinary.editReport', ['report' => $report, 'animals' => $animals]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'date' => 'required|date_format:d/m/Y',
            'animal_id' => 'required|exists:animals,id',
            'detail' => 'required|string',
        ]);

        $report = VeterinaryReport::findOrFail($id);
        $report->date = \DateTime::createFromFormat('d/m/Y', $request->date)->format('Y-m-d');
        $report->animal_id = $request->animal_id;
        $report->detail = $request->detail;
        $report->save();

        return redirect()->route('veterinaryReports.index')->with('success', 'Compte rendu mis à jour avec succès.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $report = VeterinaryReport::findOrFail($id);
        $report->delete();
        
        return redirect()->route('veterinaryReports.index')->with('success', 'Compte rendu supprimé avec succès.');
    }
}
