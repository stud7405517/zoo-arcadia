<?php

namespace App\Http\Controllers;

use App\Models\Animal;
use App\Models\Race;
use App\Models\Habitat;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreAnimalRequest;
use App\Http\Requests\UpdateAnimalRequest;

class AnimalController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    public function indexGestionAnimal()
    {
        $animaux = Animal::with('habitat', 'race')->get();
        return view('gestion.indexAnimal',['animaux' => $animaux]);
    }

    public function indexDashboardAnimal(Request $request)
    {
        $query = Animal::query();

        if ($request->has('animal_id') && !empty($request->input('animal_id'))) {
            $query->where('id', $request->input('animal_id'));
        }

        $animaux = $query->get();
        return view('dashboardAdmin.index', ['animaux' => $animaux, 'animals' => Animal::all()]);
    }

    public function incrementView($id)
    {
        $animal = Animal::findOrFail($id);
        $animal->increment('scoreView');
        return response()->json(['message' => 'Success', 'scoreView' => $animal->scoreView]);
    }
    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $races = Race::all();
        $habitats = Habitat::all();
        return view('gestion.createAnimal', compact('races', 'habitats'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreAnimalRequest $request)
    {
        $request->validate([
            'firstname' => 'required|string|max:255',
            'race_id' => 'required|exists:races,id',
            'habitat_id' => 'required|exists:habitats,id',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
    
        $animal = new Animal();
        $animal->firstname = $request->input('firstname');
        $animal->race_id = $request->input('race_id');
        $animal->habitat_id = $request->input('habitat_id');
        $animal->status = 'new';
        $animal->scoreView = 0;
    
        if ($request->hasFile('image')) {
            $habitatId = $request->input('habitat_id');
            $habitatFolder = '';
            switch ($habitatId) {
                case 1:
                    $habitatFolder = 'Jungle';
                    break;
                case 2:
                    $habitatFolder = 'Savane';
                    break;
                case 3:
                    $habitatFolder = 'Marais';
                    break;
            }
            $imageName = time() . '.' . $request->file('image')->extension();
            $request->file('image')->move(public_path('img/' . $habitatFolder), $imageName);
            $animal->image_path = 'img/' . $habitatFolder . '/' . $imageName;
        }
    
        $animal->save();
    
        return redirect()->route('gestionDesAnimaux')->with('success', 'Animal ajouté avec succès.');
    }
    

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $animal = Animal::find($id);
        $races = Race::all();
        $habitats = Habitat::all();
        return view('gestion.showAnimal',['animal' => $animal, 'races' => $races, 'habitats' => $habitats]);
    }

    public function showAnimalDetails($id)
    {
        $animal = Animal::with(['race', 'habitat', 'veterinaryReports'])->findOrFail($id);
        return view('animals.details', ['animal' => $animal]);
    }

    public function jungle()
    {
        $animaux = Animal::where('habitat_id', 1)->get(); // 1 est l'ID de l'habitat Jungle
        return view('jungle', ['animaux' => $animaux]);
    }

    public function savane()
    {
        $animaux = Animal::where('habitat_id', 2)->get();
        return view('savane', ['animaux' => $animaux]);
    }

    public function marais()
    {
        $animaux = Animal::where('habitat_id', 3)->get();
        return view('marais', ['animaux' => $animaux]);
    }
    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Animal $animal)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateAnimalRequest $request, $id)
    {
        $animal = Animal::find($id);

        $animal->firstname = $request->input('nomAnimal');
        $animal->race()->associate($request->input('race'));
        $animal->habitat()->associate($request->input('habitat'));

        if ($request->hasFile('image')) {
            $habitatId = $request->input('habitat');
            $habitatFolder = '';
            switch ($habitatId) {
                case 1:
                    $habitatFolder = 'Jungle';
                    break;
                case 2:
                    $habitatFolder = 'Savane';
                    break;
                case 3:
                    $habitatFolder = 'Marais';
                    break;
            }
            $imagePath = $request->file('image')->store('img/' . $habitatFolder, 'public');
            $animal->image_path = $imagePath;
        }

        $animal->save();

        return redirect()->route('gestionDesAnimaux')->with('success', 'Les informations de l\'animal ont été mises à jour avec succès.');
    }
    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $animal = Animal::find($id);
        if ($animal->delete()){
            return response()->json(['message' => 'Success']);
        }else {
            return response()->json(['message' => 'Error']);
        }
    }
}
