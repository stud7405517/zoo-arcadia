<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class IsAdmin
{
    public function handle(Request $request, Closure $next)
    {
        if (auth()->check() && auth()->user()->role_id === 3) {
            return $next($request);
        }

        // Rediriger ou renvoyer une réponse pour les utilisateurs non autorisés
        return redirect('/')->with('error', 'Vous n\'avez pas les autorisations nécessaires pour accéder à cette page.');
    }
}