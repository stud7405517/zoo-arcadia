          <aside id="sidebar">
              <div class="d-flex mt-3">
                  <button class="toggle-btn" type="button">
                    <i class="bi bi-back"></i>
                  </button>
                  <div class="sidebar-logo">
                      <a href="../admin">Espace administrateur</a>
                  </div>
              </div>
              <ul class="sidebar-nav">
                  <li class="sidebar-item">
                      <a href="../ajout-Compte" class="sidebar-link">
                        <i class="bi bi-person-gear"></i>
                          <span>Création de compte</span>
                      </a>
                  </li>
                  <li class="sidebar-item">
                      <a href="#" class="sidebar-link collapsed has-dropdown" data-bs-toggle="collapse"
                          data-bs-target="#auth" aria-expanded="false" aria-controls="auth">
                          <i class="bi bi-journal-check"></i>
                          <span>Gestion</span>
                      </a>
                      <ul id="auth" class="sidebar-dropdown list-unstyled collapse" data-bs-parent="#sidebar">
                          <li class="sidebar-item">
                              <a href="../gestionDesServices" class="sidebar-link">Gestion des services</a>
                          </li>
                          <li class="sidebar-item">
                              <a href="../gestionDesHabitats" class="sidebar-link">Gestion des habitats</a>
                          </li>
                          <li class="sidebar-item">
                              <a href="../gestionDesAnimaux" class="sidebar-link">Gestion des animaux</a>
                          </li>
                          <li class="sidebar-item">
                              <a href="../gestionDesRaces" class="sidebar-link">Gestion des races</a>
                          </li>
                      </ul>
                  </li>
                  <li class="sidebar-item">
                      <a href="../indexHoraires" class="sidebar-link">
                        <i class="bi bi-clock-history"></i>
                          <span>Horaires</span>
                      </a>
                  </li>
                  <li class="sidebar-item">
                      <a href="../indexComptesRendus" class="sidebar-link">
                        <i class="bi bi-chat-square-text"></i>
                          <span>Comptes rendus</span>
                      </a>
                  </li>
              </ul>
          </aside>