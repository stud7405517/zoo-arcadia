<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Page d'accueil - Arcadia Zoo</title>
    <link rel="stylesheet" href="scss/main.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Jolly+Lodger&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Karla:ital@0;1&display=swap" rel="stylesheet">
</head>
<body>

@extends('layout')

@section('contenu')

    <main>
        <!-- Contenu principal de la page -->
        <article class="bg-success text-white">
          <div class="container p-4">
            <h2 class="text-center text-primary mb-4">Bienvenue au Zoo Arcadia</h2>
            <div class="align-items-center">
                <div class="col">
                    <p class="text-center text-justify mb-4">
                      Découvrez la magie de la nature près de la forêt de Brocéliande, en Bretagne, depuis 1960.
                    </p>
                </div>
            </div>

            <div id="carouselExampleIndicators" class="carousel slide mb-5" data-bs-ride="carousel">
              <div class="carousel-indicators">
                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="3" aria-label="Slide 4"></button>
              </div>
              <div class="carousel-inner">
                <div class="carousel-item active">
                  <img src="../img/Slider/lion.jpeg" class="d-block w-50 rounded mx-auto d-block" alt="First slide">
                </div>
                <div class="carousel-item">
                  <img src="../img/Slider/rinhoceros.jpg" class="d-block w-50 rounded mx-auto d-block" alt="Second slide">
                </div>
                <div class="carousel-item">
                  <img src="../img/Slider/zebre.jpeg" class="d-block w-50 rounded mx-auto d-block" alt="Third slide">
                </div>
                <div class="carousel-item">
                  <img src="../img/Slider/idk.webp" class="d-block w-50 rounded mx-auto d-block" alt="Fourth slide">
                </div>
              </div>
              <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
              </button>
              <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
              </button>
            </div>
          </div>
        </article>

        <article class="bg-dark text-white">
          <div class="container p-4">
            <h2 class="text-center text-primary mb-4">Nos habitats et nos services</h2>
            <div class="align-items-center">
              <div class="col">
                <p class="text-center text-justify mb-4">
                Explorez nos habitats uniques où vivent une variété d'animaux fascinants. De la majestueuse savane à la mystérieuse jungle en passant par les marais enchantés, chaque coin du zoo abrite une diversité incroyable de créatures.
                </p>
              </div>
            
              <div class="row row-cols-2 align-items-center">
                <div class="col">
                  <p class="text-justify">
                  <img src="../img/accueil hs/Savane éléphants.png" class="d-block w-75 rounded mx-auto d-block">
                  <div class="text-center pt-4">
                  <a href="savane" class="btn btn-primary mb-5">La Savane</a>
                  </div>
                  <img src="../img/accueil hs/Jungle monkey.png" class="d-block w-75 rounded mx-auto d-block">
                  <div class="text-center pt-4">
                  <a href="jungle" class="btn btn-primary mb-5">La Jungle</a>
                  </div>
                </div>
                <div class="col">
                  <p class="text-justify">
                  <img src="../img/accueil hs/Marais Crocodiles.png" class="d-block w-75 rounded mx-auto d-block">
                  <div class="text-center pt-4">
                  <a href="marais" class="btn btn-primary mb-5">Les Marais</a>
                  </div>
                  <img src="../img/accueil hs/Services Petits trains.png" class="d-block w-75 rounded mx-auto d-block">
                  <div class="text-center pt-4">
                  <a href="services" class="btn btn-primary mb-5">Nos Services</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </article>

        <article class="bg-secondary text-white">
          <div class="container-lg p-4">
            <div class="row align-items-center">
              <div class="col-lg-3 text-center">
                <img src="img/Feuille.png" alt="Image gauche">
              </div>
              <div class="col-lg-6 ">
                <p class="text-center text-justify my-auto">
                  Au Zoo Arcadia, nous prenons soins de nos animaux et de notre planète.
                </p>
              </div>
              <div class="col-lg-3 text-center">
                <img src="img/Feuille.png" alt="Image droite">
              </div>
            </div>
          </div>
        </article>

        <article class="bg-light text-white">
          <div class="container p-4">
            <h2 class="text-center text-info mb-4">Avis de nos visiteurs</h2>
            <div class="align-items-center">
              <div id="carouselExampleControls" class="carousel slide mb-4" data-bs-ride="carousel">
                <div class="carousel-inner">
                  <div class="carousel-item active">
                    <div class="container">
                      <div class="row justify-content-center">
                        <div class="col-md-8">
                          <div class="card">
                            <div class="card-body">
                              <h5 class="card-title">Avis 1</h5>
                              <p class="card-text">Contenu de l'avis 1.</p>
                              <class="text-muted">Posté le <span class="fw-bold">[Date du jour]</span> <span class="float-end">[Pseudo]</span></small>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="carousel-item">
                    <div class="container">
                      <div class="row justify-content-center">
                        <div class="col-md-8">
                          <div class="card">
                            <div class="card-body">
                              <h5 class="card-title">Avis 2</h5>
                              <p class="card-text">Contenu de l'avis 2.</p>
                              <class="text-muted">Posté le <span class="fw-bold">[Date du jour]</span> <span class="float-end">[Pseudo]</span></small>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- Ajoutez plus d'items ici au besoin -->
                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="text-info">Précédent</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="text-info">Suivant</span>
                </button>
              </div>
            </div>
          </div>
          <div class="text-center pt-4">
                  <a href="avis" class="btn btn-info mb-5">Donnez-nous votre avis !</a>
                  </div>
        </article>  
        
        <article>
          <div class="container-fluid bg-light py-4">
            <div class="row justify-content-center">
              <div class="col-2 text-center">
                <a href="#"><i class="bi bi-facebook fs-3 text-info"></i></a>
              </div>
              <div class="col-2 text-center">
                <a href="#"><i class="bi bi-twitter fs-3 text-info"></i></a>
              </div>
              <div class="col-2 text-center">
                <a href="#"><i class="bi bi-instagram fs-3 text-info"></i></a>
              </div>
              <div class="col-2 text-center">
                <a href="#"><i class="bi bi-linkedin fs-3 text-info"></i></a>
              </div>
              <div class="col-2 text-center">
                <a href="#"><i class="bi bi-youtube fs-3 text-info"></i></a>
              </div>
            </div>
          </div>
        </article>
    </main>

@endsection

    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
      integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
      crossorigin="anonymous"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>