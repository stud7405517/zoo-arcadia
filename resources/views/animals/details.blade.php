<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Détails de l'Animal - Arcadia Zoo</title>
    <link rel="stylesheet" href="{{ asset('scss/main.css') }}">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Jolly+Lodger&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Karla:ital@0;1&display=swap" rel="stylesheet">
</head>
<body>

@extends('layout')

@section('contenu')

<main>
    <article class="bg-warning text-dark">
        <div class="container p-4">
        <div class="container">
            <h2 class="text-center text-whitery mb-4">{{ $animal->firstname }}</h2>
            <div class="row justify-content-center">
                <div class="col-auto text-center text-whitery mb-4">
                    Race: {{ $animal->race->name }}
                </div>
                <div class="col-auto text-center text-whitery mb-4">
                    Habitat: {{ $animal->habitat->name }}
                </div>
            </div>
            <img src="{{ asset($animal->image_path) }}" alt="{{ $animal->firstname }}" class="mx-auto d-block mb-4">

            <h2 class="text-center text-whitery mb-4">Comptes rendus vétérinaires</h2>
            @if($animal->veterinaryReports->isEmpty())
                <p>Aucun compte rendu disponible pour cet animal.</p>
            @else
                <ul>
                    @foreach($animal->veterinaryReports as $report)
                        <li>
                            <strong>Date:</strong> {{ \Carbon\Carbon::parse($report->date)->format('d/m/Y') }}<br>
                            <strong>Détails:</strong> {{ $report->detail }}
                        </li>
                        <br>
                    @endforeach
                </ul>
            @endif
        </div>
        <p>Nombre de vues : <span id="view-count">{{ $animal->scoreView }}</span></p>
    </article>
@include('environnement_rs')
</main>

@endsection
<script>
        document.addEventListener('DOMContentLoaded', function() {
            const animalId = {{ $animal->id }};
            fetch(`/animal/${animalId}/increment-view`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
            })
            .then(response => response.json())
            .then(data => {
                document.getElementById('view-count').textContent = data.scoreView;
            })
            .catch(error => console.error('Error:', error));
        });
    </script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe" crossorigin="anonymous"></script>
<script src="{{ asset('js/script.js') }}"></script>
</body>
</html>