<article class="bg-secondary text-white">
          <div class="container-lg p-4">
            <div class="row align-items-center">
              <div class="col-lg-3 text-center">
                <img src="../img/Feuille.png" alt="Image gauche">
              </div>
              <div class="col-lg-6 ">
                <p class="text-center text-justify my-auto">
                  Au Zoo Arcadia, nous prenons soins de nos animaux et de notre planète.
                </p>
              </div>
              <div class="col-lg-3 text-center">
                <img src="../img/Feuille.png" alt="Image droite">
              </div>
            </div>
          </div>
        </article>

        <article>
          <div class="container-fluid bg-light py-4">
            <div class="row justify-content-center">
              <div class="col-2 text-center">
                <a href="#"><i class="bi bi-facebook fs-3 text-info"></i></a>
              </div>
              <div class="col-2 text-center">
                <a href="#"><i class="bi bi-twitter fs-3 text-info"></i></a>
              </div>
              <div class="col-2 text-center">
                <a href="#"><i class="bi bi-instagram fs-3 text-info"></i></a>
              </div>
              <div class="col-2 text-center">
                <a href="#"><i class="bi bi-linkedin fs-3 text-info"></i></a>
              </div>
              <div class="col-2 text-center">
                <a href="#"><i class="bi bi-youtube fs-3 text-info"></i></a>
              </div>
            </div>
          </div>
        </article>