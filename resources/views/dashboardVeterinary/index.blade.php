<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gestion des comptes rendus - Arcadia Zoo</title>
    <link rel="stylesheet" href="{{ asset('scss/main.css') }}">
</head>
<body>

@include('header')

<main>
    <div class="wrapper">
        @include('sidebarVeterinary')
        <div class="main p-3">
            <section id="reports">
                <div class="card">
                    <h3 class="card-header text-center text-info mb-4">Gestion des comptes rendus</h3>
                    <div class="card-body px-4">
                        <div class="text-center pt-4">
                            <a href="{{ route('veterinaryReports.create') }}" class="btn btn-info mb-5">Ajouter un compte rendu</a>
                        </div>
                        <div class="container">
                        <table class="table table-striped table-hover">
                          <thead>
                              <tr class="table-warning">
                                  <th class="text-center">Date</th>
                                  <th class="text-center">Animal</th>
                                  <th class="text-center">Comptes rendus</th>
                                  <th class="text-center"></th>
                                  <th class="text-center"></th>
                              </tr>
                          </thead>
                          <tbody>
                              @foreach($reports as $report)
                                  <tr>
                                      <td class="text-center">{{ \Carbon\Carbon::parse($report->date)->format('d/m/Y') }}</td>
                                      <td class="text-center">{{ $report->animal->firstname ?? 'Animal non trouvé' }}</td>
                                      <td class="text-center">{{ $report->detail }}</td>
                                      <td class="px-4 py-2 text-center">
                                          <form action="{{ route('veterinaryReports.destroy', $report->id) }}" method="post">
                                              @method('delete')
                                              @csrf
                                              <button type="submit" class="btn btn-danger">Supprimer</button>
                                          </form>
                                      </td>
                                      <td class="text-center">
                                          <a href="{{ route('veterinaryReports.edit', $report->id) }}" class="btn btn-primary">Modifier</a>
                                      </td>
                                  </tr>
                              @endforeach
                          </tbody>
                      </table>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</main>

<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
      integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
      crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe"
      crossorigin="anonymous"></script>
<script src="node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<script src="js/script.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>
<script src="{{ asset('js/script.js') }}"></script>
</body>
</html>