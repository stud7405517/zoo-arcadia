<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Consultation de la nourriture donnée aux animaux - Arcadia Zoo</title>
    <link rel="stylesheet" href="{{ asset('scss/main.css') }}">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Jolly+Lodger&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Karla:ital@0;1&display=swap" rel="stylesheet">
</head>
<body>
@include('header')

<main>
    <div class="wrapper">
        @include('sidebar')
        <div class="main p-3">
            <section id="notes-form">
                <div class="card">
                    <h3 class="card-header text-center text-info mb-4">Consultation de la nourriture donnée aux animaux</h3>
                    <div class="card-body px-4">
                        <form method="GET" action="{{ route('veterinary.employee.index') }}">
                            <div class="container">
                                <label>Selectionner un animal</label>
                                <select class="form-select mb-4" name="animal_id" aria-label="Default select example">
                                    <option value="">Tous les animaux</option>
                                    @foreach($animals as $animal)
                                        <option value="{{ $animal->id }}">{{ $animal->firstname }}</option>
                                    @endforeach
                                </select>

                                <label for="startDate">Date de départ</label>
                                <input id="startDate" class="mb-4 form-control" type="date" name="start_date" />

                                <label for="endDate">Date de fin</label>
                                <input id="endDate" class="mb-4 form-control" type="date" name="end_date" />

                                <div class="d-flex justify-content-center">
                                    <button type="submit" class="btn btn-primary mb-4">Filtrer</button>
                                </div>
                            </div>
                        </form>

                        <table class="table table-striped table-hover">
                            <thead>
                                <tr class="table-warning">
                                    <th>Date</th>
                                    <th>Heure</th>
                                    <th>Animal</th>
                                    <th>Nourriture</th>
                                    <th>Quantité</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($nourritures as $nourriture)
                                    <tr>
                                        <td>{{ \Carbon\Carbon::parse($nourriture->date)->format('d/m/Y') }}</td>
                                        <td>{{ $nourriture->heure }}</td>
                                        <td>{{ $nourriture->animal->firstname }}</td>
                                        <td>{{ $nourriture->detail }}</td>
                                        <td>{{ $nourriture->quantité }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
</main>

<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
      integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
      crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe"
      crossorigin="anonymous"></script>
<script src="node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<script src="js/script.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>
<script src="{{ asset('js/script.js') }}"></script>
</body>
</html>