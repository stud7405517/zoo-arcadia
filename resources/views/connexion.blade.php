<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Connexion - Arcadia Zoo</title>
    <link rel="stylesheet" href="scss/main.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Jolly+Lodger&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Karla:ital@0;1&display=swap" rel="stylesheet">
</head>
<body>

@extends('layout')

@section('contenu')

<main>
    <!-- Contenu principal de la page -->
    <article class="text-black">
        <div class="container-lg p-4">
            <h2 class="text-center text-info mb-4">Connexion</h2>
            <div class="container">
                <form action="/connexion" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="email">Entrez votre mail</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="test@gmail.com" value="{{ old('email') }}">
                        @if($errors->has('email'))
                        <p class="help is-danger">{{ $errors->first('email') }}</p>
                        @endif
                    </div> 
                    <div class="form-group">
                        <label for="password">Mot de passe</label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="Entrez votre mot de passe">
                        @if($errors->has('password'))
                        <p class="help is-danger">{{ $errors->first('password') }}</p>
                        @endif
                    </div>
                    <div class="text-center pt-4">
                        <input type="submit" class="btn btn-info mb-5" value="Se connecter">
                    </div>
                </form>
            </div>
        </div>
    </article>
    @include('environnement_rs')
</main>

@endsection

    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
      integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
      crossorigin="anonymous"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>