<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gestion des services - Arcadia Zoo</title>
    <link rel="stylesheet" href="scss/main.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Jolly+Lodger&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Karla:ital@0;1&display=swap" rel="stylesheet">
</head>
<body>

@include('header')

    <main>
      <div class="wrapper">
        @include('sidebar')
          <div class="main p-3">
            <section id="notes-form">
                <div class="card">
                  <h3 class="card-header text-center text-info mb-4">Gestion des services</h3>
                  <div class="card-body px-4">
                  
                  <div class="text-center pt-4">
                    <a href="{{ route('gestionDesServices.create') }}" class="btn btn-info mb-5">Ajouter un service</a>
                  </div>

                    <div class="container">
                        <table class="table table-striped table-hover">
                          <thead>
                            <tr class="table-warning">
                              <th class="text-center">Nom du service</th>
                              <th class="text-center">Description</th>
                              <th class="text-center"></th>
                              <th class="text-center"></th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach($services as $service)
                              <tr>
                                <td>{{$service->name}}</td>
                                <td>{{$service->description}}</td>
                                <!-- <td>Lien</td> -->
                                <td class="px-4 py-2 text-center">
                                  <form action="{{route('gestionDesServices.destroy',$service->id)}}" method="post">
                                    @method('delete')
                                    @csrf
                                    <button type="submit" class="btn btn-danger">Supprimer</button>
                                  </form>
                                </td>
                                <td>
                                  <a href="{{ route('gestionDesServices.show',$service->id) }}" class="btn btn-primary">Modifier ce service</a>
                                </td>
                              </tr>
                            @endforeach
                          </tbody>
                        </table>
                      </div>
                      </fieldset>
                    </div>
                  </div>
                </div>
              </section>
          </div>
          </div>
      </div>
    </main>

    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
      integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
      crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe"
      crossorigin="anonymous"></script>
  <script src="node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script src="js/script.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>
  <script src="{{ asset('js/script.js') }}"></script>
</body>
</html>