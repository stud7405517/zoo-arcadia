<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Modifier la nourriture donnée - Arcadia Zoo</title>
    <link rel="stylesheet" href="{{ asset('scss/main.css') }}">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
</head>
    <body>
        @include('header')
        <main>
            <div class="wrapper">
                @include('sidebarEmployee')
                <div class="main p-3">
                    <section id="notes-form">
                        <div class="card">
                            <h3 class="card-header text-center text-info mb-4">Modifier la nourriture donnée</h3>
                            <div class="card-body px-4 text-center">
                                <form action="{{ route('employee.update', $nourriture->id) }}" method="post">
                                    @csrf
                                    @method('PUT')
                                    <label for="date">Date :</label><br>
                                    <input type="text" id="date" name="date" value="{{ \Carbon\Carbon::parse($nourriture->date)->format('d/m/Y') }}" required><br>
                                    <div class="form-group">
                                        <label for="heure">Heure</label>
                                        <input type="time" class="form-control" id="heure" name="heure" value="{{ $nourriture->heure }}" required>
                                    </div>
                                    <label for="animal_id">Animal :</label><br>
                                    <select class="form-select" id="animal_id" name="animal_id" required>
                                        <option value="" disabled>Choisissez un animal</option>
                                        @foreach($animals as $animal)
                                            <option value="{{ $animal->id }}" {{ $nourriture->animal_id == $animal->id ? 'selected' : '' }}>{{ $animal->firstname }}</option>
                                        @endforeach
                                    </select>
                                    <label for="detail">Nourriture donnée :</label><br>
                                    <textarea id="detail" name="detail" rows="4" cols="50" required>{{ $nourriture->detail }}</textarea><br><br>
                                    <label for="quantité">Quantité :</label><br>
                                    <textarea id="quantité" name="quantité" rows="4" cols="50" required>{{ $nourriture->quantité }}</textarea><br><br>
                                    <input type="submit" value="Modifier la nourriture donnée">
                                </form>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </main>
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script>
            $(document).ready(function() {
                $("#date").datepicker({
                    dateFormat: 'dd/mm/yy'
                });
            });
        </script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe" crossorigin="anonymous"></script>
        <script src="node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
        <script src="js/script.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>
        <script src="{{ asset('js/script.js') }}"></script>
    </body>
</html>