<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>La Jungle - Arcadia Zoo</title>
    <link rel="stylesheet" href="scss/main.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Jolly+Lodger&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Karla:ital@0;1&display=swap" rel="stylesheet">
</head>
<body>

@extends('layout')

@section('contenu')

<main>
    <!-- Contenu principal de la page -->
    <div class="masthead w-100" style="background-image: url('./img/Jungle/jungle.png');">
        <div class="color-overlay d-flex justify-content-center align-items-center"></div>
    </div>

    <article class="bg-warning text-dark">
        <div class="container p-4">
            <h2 class="text-center text-whitery mb-4">La Jungle</h2>
            <div class="align-items-center">
                <div class="col">
                    <p class="text-center text-justify mb-4">
                        La jungle luxuriante est le foyer de nombreuses espèces exotiques, y compris les singes, les tigres, les léopards, les jaguars et une multitude d'autres animaux fascinants. Plongez dans la densité de la jungle et découvrez la diversité incroyable de la faune qui y habite.
                    </p>
                </div>
            </div>
        </div>

        <article class="bg-dark text-dark">
            <div class="container p-4">
                <div class="align-items-center">
                    <div class="col text-center">
                        <div class="container mt-5">
                            <div class="row justify-content-center">
                                <div class="col-12 col-md-6">
                                    <h2 class="custom-box-warning text-center mb-5">Nos animaux</h2>
                                </div>
                            </div>
                            <div class="row row-cols-2 align-items-center">
                                @foreach($animaux as $animal)
                                <div class="col">
                                    <p class="text-justify">
                                    <img src="{{ asset($animal->image_path) }}" class="d-block w-75 rounded mx-auto d-block">
                                    <!-- <img src="../img/Jungle/Jungle.png" class="d-block w-75 rounded mx-auto d-block"> -->
                                    <div class="text-center pt-4">
                                    <a href="{{ route('animal.details', $animal->id) }}" class="btn btn-warning mb-5">{{ $animal->firstname }}</a>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </article>

    @include('environnement_rs')
</main>

@endsection  

<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
    integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
    crossorigin="anonymous"></script>
<script src="node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>