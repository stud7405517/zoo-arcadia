<header>
    <nav class="navbar navbar-expand-lg bg-dark" data-bs-theme="dark">
        <div class="container-fluid">
            <a class="navbar-brand" href="/">
                <img src="../img/Logo Zoo Arcadia.png" alt="logo" class="logo" />
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link" href="/">Accueil</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../services">Services</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Habitats
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="../habitats">Tous nos habitats</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="../savane">Savane</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="../jungle">Jungle</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="../marais">Marais</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../contact">Contact</a>
                    </li>
                    @guest
                        <li class="nav-item">
                            <a class="nav-link" href="../connexion">Connexion</a>
                        </li>
                    @endguest
                    @auth
                        <li class="nav-item">
                            <form action="{{ route('logout') }}" method="POST" style="display: inline;">
                                @csrf
                                <button type="submit" class="nav-link btn btn-link" style="cursor: pointer;">Déconnexion</button>
                            </form>
                        </li>
                    @endauth
                    <li class="nav-item">
                        <a class="nav-link" href="../admin">Espace Administrateur</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../veterinary">Espace Vétérinaire</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../employee">Espace Employé</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>

@yield('contenu')

<footer class="bg-dark text-white text-center text-lg-start">
    <!-- Pied de page -->
    <div class="container-lg py-2">
        <div class="row">
            <div class="col-12 col-lg-4 my-auto">
                <img class="w-auto rounded" src="../img/Logo Zoo Arcadia.png" />
                <br />
                <h3>Nos services</h3>
                <p>Visites guidées<br />
                    Restauration<br />
                    Petit train<br />
                </p>
            </div>
            <div class="col-6 col-lg-4 my-auto">
                <h3>Nos horaires</h3>
                <p>Lundi 9:00-19:00<br />
                    Mardi 9:00-19:00 <br />
                    Mercredi 9:00-19:00 <br />
                    Jeudi 9:00-19:00<br />
                    Vendredi 9:00-19:00<br />
                    Samedi 9:00-20:00<br />
                    Dimanche 9:00-19:00<br />
                </p>
            </div>
            <div class="col-6 col-lg-4 my-auto">
                <h3>Nos coordonnées : </h3>
                <p>06 1 23 45 67 89</p>
                <p>info@zooarcadia.com</p>
                <p>123 Rue des Animaux, 35000, France</p>
            </div>
        </div>
    </div>
    <!-- Copyright -->
    <div class="text-center p-4" style="background-color: rgba(0, 0, 0, 0.05);">
        © 2024 Arcadia Zoo - Tous droits réservés
    </div>
</footer>