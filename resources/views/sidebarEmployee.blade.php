          <aside id="sidebar">
              <div class="d-flex mt-3">
                  <button class="toggle-btn" type="button">
                    <i class="bi bi-back"></i>
                  </button>
                  <div class="sidebar-logo">
                      <a href="../">Espace employé</a>
                  </div>
              </div>
              <ul class="sidebar-nav">
                  <li class="sidebar-item">
                      <a href="/employee" class="sidebar-link">
                        <i class="bi bi-egg-fried"></i>
                          <span>Gestion de la nourriture</span>
                      </a>
                  </li>
                  <li class="sidebar-item">
                      <a href="../gestionDesServices" class="sidebar-link">
                        <i class="bi bi-journal-check"></i>
                          <span>Gestion des services</span>
                      </a>
                  </li>
                  <li class="sidebar-item">
                      <a href="#" class="sidebar-link">
                        <i class="bi bi-chat-square-text"></i>
                          <span>Gestion des avis</span>
                      </a>
                  </li>
              </ul>
          </aside>