<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Nos Services - Arcadia Zoo</title>
    <link rel="stylesheet" href="scss/main.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Jolly+Lodger&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Karla:ital@0;1&display=swap" rel="stylesheet">
</head>
<body>

@extends('layout')

@section('contenu')

    <main>
        <!-- Contenu principal de la page -->
        <article class="bg-success text-white">
          <div class="container-lg p-4">
            <h2 class="text-center text-primary mb-4">Nos Services</h2>
            <div class="align-items-center">
              <div class="row row-cols-2 align-items-center">
                <div class="col">
                  <img class="rounded-circle w-75 mx-auto d-block" alt="Photo Savane éléphants" src="../img/Services/Vue du ciel.png"/>
                </div>
                <div class="col">
                  <p class="text-center">
                    Au Zoo Arcadia, nous nous efforçons de vous offrir une expérience complète et mémorable lors de votre visite.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </article>
        <article class="bg-success text-white">
          <div class="container-lg p-4">
            <div class="text-center mb-4">
                  <p class="text-justify mb-4">
                  Découvrez ci-dessous les différents services que nous proposons pour enrichir votre expérience :</p>
                </div>
          </div>
        </article>

        <article class="bg-dark text-white">
          <div class="container-lg p-4">
            <h2 class="text-center text-primary mb-4">Restaurant</h2>
            <div class="text-center mb-4">
              <p class="text-justify mb-4">
                Notre zone de restauration propose une variété de délicieux plats et collations pour combler toutes les envies. Détendez-vous et profitez d'une pause gourmande tout en admirant la vue sur nos habitats uniques.
              </p>
              <img class="mx-auto w-75 d-block" alt="Photo Savane éléphants" src="../img/Services/Restaurant intérieur.png"/>
            </div>
          </div>
        </article>

        <article class="bg-success text-white">
          <div class="container-lg p-4">
            <h2 class="text-center text-primary mb-4">Nos visites guidées</h2>
            <div class="text-center mb-4">
              <p class="text-justify mb-4">
              Participez à nos visites guidées gratuites pour découvrir les merveilles de notre zoo en compagnie de nos guides experts. Apprenez-en davantage sur nos animaux, leur habitat naturel et les efforts de conservation que nous menons.</p>
              <img class="mx-auto w-75 d-block" alt="Photo Savane éléphants" src="../img/Services/Guide Zoo visiteurs.png"/>
            </div>
          </div>
        </article>

        <article class="bg-dark text-white">
          <div class="container-lg p-4">
            <h2 class="text-center text-primary mb-4">Petits trains</h2>
            <div class="text-center mb-4">
              <p class="text-justify mb-4">
              Embarquez à bord de notre petit train et parcourez notre parc de manière confortable et ludique. Profitez d'une vue panoramique sur nos différentes zones et écoutez les commentaires de nos conducteurs sur les animaux que vous rencontrerez en chemin.</p>
              <img class="mx-auto w-75 d-block" alt="Photo Savane éléphants" src="../img/Services/Petit train.png"/>
            </div>
          </div>
        </article>
      @include('environnement_rs')
    </main>

@endsection

    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
      integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
      crossorigin="anonymous"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>