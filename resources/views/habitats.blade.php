<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Les habitats - Arcadia Zoo</title>
    <link rel="stylesheet" href="scss/main.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Jolly+Lodger&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Karla:ital@0;1&display=swap" rel="stylesheet">
</head>
<body>

@extends('layout')

@section('contenu')

    <main>
        <!-- Contenu principal de la page -->
        <article class="bg-dark text-white">
          <div class="container-lg p-4">
            <h2 class="text-center text-primary mb-4">Nos Habitats</h2>
            <div class="align-items-center">
              <div class="row row-cols-2 align-items-center">
                <div class="col">
                  <img class="rounded-circle w-75 mx-auto d-block" alt="Photo de la tête d'une girafe" src="../img/Habitats/Girafe.png"/>
                </div>
                <div class="col">
                  <p class="text-center">
                    Au Zoo Arcadia, nos 3 habitats splendides sont conçus pour le bien-être de nos animaux.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </article>
        <article class="bg-dark text-white">
          <div class="container-lg p-4">
            <div class="text-center mb-4">
                <p class="text-justify mb-4">
                Découvrez ci-dessous les différents habitats fascinants que vous pouvez explorer au Zoo Arcadia, ainsi que les merveilleux animaux qui y résident :</p>
              </div>
          </div>
        </article>

        <article class="bg-success text-white">
          <div class="container-lg p-4">
            <h2 class="text-center text-primary mb-4">La Savane</h2>
            <div class="text-center mb-4">
              <p class="text-justify mb-4">
              La savane, vaste étendue herbeuse parsemée d'arbres et de buissons, abrite une variété d'animaux majestueux tels que les lions, les éléphants, les girafes, les zèbres et bien d'autres. Imprégnez-vous de l'ambiance de la savane africaine et observez ces créatures emblématiques évoluer dans leur environnement naturel.</p>
              <img class="mx-auto w-75 d-block" alt="Photo Savane lionne" src="../img/Habitats/Savane.png"/>
            </div>
            <div class="text-center pt-4">
              <a href="savane" class="btn btn-primary mb-5">La Savane</a>
            </div>
          </div>
        </article>

        <article class="bg-dark text-white">
          <div class="container-lg p-4">
            <h2 class="text-center text-primary mb-4">La Jungle</h2>
            <div class="text-center mb-4">
              <p class="text-justify mb-4">
              La jungle luxuriante est le foyer de nombreuses espèces exotiques, y compris les singes, les tigres, les léopards, les jaguars et une multitude d'autres animaux fascinants. Plongez dans la densité de la jungle et découvrez la diversité incroyable de la faune qui y habite.</p>
              <img class="mx-auto w-75 d-block" alt="Photo Jungle Singe" src="../img/Habitats/Jungle.png"/>
            </div>
            <div class="text-center pt-4">
              <a href="jungle" class="btn btn-primary mb-5">La Jungle</a>
            </div>
          </div>
        </article>

        <article class="bg-success text-white">
          <div class="container-lg p-4">
            <h2 class="text-center text-primary mb-4">Les Marais</h2>
            <div class="text-center mb-4">
              <p class="text-justify mb-4">
              Les marais sont des écosystèmes riches en biodiversité, abritant des créatures adaptées à la vie aquatique et terrestre. Rencontrez des crocodiles, des alligators, des hérons, des tortues, des grenouilles et bien plus encore dans cet habitat unique et préservé.</p>
              <img class="mx-auto w-75 d-block" alt="Photo Marais Crocodile" src="../img/Habitats/Marais.png"/>
            </div>
            <div class="text-center pt-4">
              <a href="marais" class="btn btn-primary mb-5">Les Marais</a>
            </div>
          </div>
        </article>


      @include('environnement_rs')
    </main>

@endsection 

    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
      integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
      crossorigin="anonymous"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>