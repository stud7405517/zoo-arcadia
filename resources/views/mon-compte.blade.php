<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mon compte - Arcadia Zoo</title>
    <link rel="stylesheet" href="scss/main.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Jolly+Lodger&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Karla:ital@0;1&display=swap" rel="stylesheet">
</head>
<body>

@extends('layout')

@section('contenu')

    <main>
        <!-- Contenu principal de la page -->
        <article class="text-black">
          <div class="container-lg p-4">
            <h2 class="text-center text-info mb-4">Mon compte</h2>
            <div class="container">
              <div class="section">
                <h1 class="tilte is-1">Mon compte</h1>

                <p>Vous êtes bien connecté, vous pouvez-vous rediriger vers une nouvelle page.</p>

                <a href="/deconnexion" class="button">Déconnexion</a>
          </div>
        </article>
      @include('environnement_rs')
    </main>

@endsection

    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
      integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
      crossorigin="anonymous"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>