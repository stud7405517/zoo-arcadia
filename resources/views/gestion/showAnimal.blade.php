<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Modifier un animal - Arcadia Zoo</title>
    <link rel="stylesheet" href="../scss/main.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Jolly+Lodger&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Karla:ital@0;1&display=swap" rel="stylesheet">
</head>
<body>

@include('header')

    <main>
      <div class="wrapper">
        @include('sidebar')
          <div class="main p-3">
            <section id="notes-form">
                <div class="card">
                  <h3 class="card-header text-center text-info mb-4">Gestion des animaux</h3>
                  <div class="card-body px-4 text-center">
                  <form action="{{ route('gestionDesAnimaux.update', $animal->id) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="mb-3">
                        <label for="nomAnimal" class="form-label">Nom de l'animal</label>
                        <input type="text" class="form-control" id="nomAnimal" name="nomAnimal" value="{{ $animal->firstname }}">
                    </div>
                    <div class="mb-3">
                        <label for="race" class="form-label">Race</label>
                        <select class="form-select" id="race" name="race">
                            @foreach($races as $race)
                                <option value="{{ $race->id }}" {{ $race->id == $animal->race_id ? 'selected' : '' }}>{{ $race->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="mb-3">
                        <label for="habitat" class="form-label">Habitat</label>
                        <select class="form-select" id="habitat" name="habitat">
                            @foreach($habitats as $habitat)
                                <option value="{{ $habitat->id }}" {{ $habitat->id == $animal->habitat_id ? 'selected' : '' }}>{{ $habitat->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="mb-3">
                        <label for="image" class="form-label">Image de l'animal</label>
                        <input type="file" class="form-control" id="image" name="image">
                        @if($animal->image_path)
                            <img src="{{ asset('storage/' . $animal->image_path) }}" alt="{{ $animal->firstname }}" class="img-fluid mt-3">
                        @endif
                    </div>
                    <button type="submit" class="btn btn-info">Modifier l'animal</button>
                </form>
                  </div>
                </div>
            </section>
          </div>
      </div>
    </main>


    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
      integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
      crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe"
      crossorigin="anonymous"></script>
  <script src="node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script src="js/script.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>
  <script src="{{ asset('js/script.js') }}"></script>
</body>
</html>