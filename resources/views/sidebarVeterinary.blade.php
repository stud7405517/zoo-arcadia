          <aside id="sidebar">
              <div class="d-flex mt-3">
                  <button class="toggle-btn" type="button">
                    <i class="bi bi-back"></i>
                  </button>
                  <div class="sidebar-logo">
                      <a href="../veterinary">Espace vétérinaire</a>
                  </div>
              </div>
              <ul class="sidebar-nav">
                  <li class="sidebar-item">
                      <a href="../veterinary" class="sidebar-link">
                        <i class="bi bi-journal-check"></i>
                          <span>Comptes rendus</span>
                      </a>
                  </li>
                  <li class="sidebar-item">
                      <a href="../nourriture" class="sidebar-link">
                        <i class="bi bi-egg-fried"></i>
                          <span>Consultation de la nourriture</span>
                      </a>
                  </li>
              </ul>
          </aside>