# Démarche à suivre
Arcadia Zoo Web Application

Description

Arcadia est un zoo situé en France près de la forêt de Brocéliande, en Bretagne, depuis 1960. Ce projet vise à fournir une application web pour que les visiteurs puissent visualiser les animaux, leurs états, les services et les horaires du zoo. L’application contient également des espaces pour l’administrateur, les vétérinaires et les employés.

Prérequis
•	XAMPP : Version 7.4.29 ou supérieure (Apache, PhpMyAdmin (MySQL), PHP)
•	Composer : Version 2.1.3 ou supérieure
•	Laravel : Version 11.5.0
•	Bootstrap : Version 5.3

Etape 1- Démarrer le serveur

Sur XAMPP, démarrer Apache et MySQL.
Lancer l’application Laravel
php artisan serve
L’application sera accessible à l’adresse suivante : http://localhost:8000

Étape 2- Installation : Cloner le dépôt
git clone https://gitlab.com/stud7405517/zoo-arcadia/-/tree/main?ref_type=heads
cd arcadia-zoo

Étape 3- Installer les dépendances
npm install
npm run dev

Étape 4- Configurer la base de données

1. Ouvrir et configurer le fichier .env avec les paramètres suivants :
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=arcadia
DB_USERNAME=root
DB_PASSWORD=

2. Créer la base de données en utilisant PhpMyAdmin ou MySQL en ligne de commande :
CREATE DATABASE arcadia_zoo

3. Exécutez les migrations et les seeders :
php artisan migrate –seeder


Fonctionnalités

Pages publiques
•	Page d’accueil : Présentation du zoo, images, habitats, services et avis.
•	Menu de navigation : Liens vers la page d’accueil, services, habitats, connexion et contact.
•	Vue globale des services : Liste de tous les services offerts par le zoo.
•	Vue globale des habitats : Liste des habitats et détails des animaux par habitat.
•	Contact : Formulaire de contact pour les visiteurs.

Espace utilisateur
•	Connexion : Authentification pour les administrateurs, vétérinaires et employés.

Espace administrateur
•	Gestion des comptes employés et vétérinaires.
•	Gestion des services, habitats et animaux.
•	Consultation des comptes rendus vétérinaires et des statistiques.

Espace employé
•	Validation des avis des visiteurs.
•	Gestion des services du zoo.
•	Ajout de la consommation de nourriture pour les animaux.

Espace vétérinaire
•	Remplissage des comptes rendus par animal.
•	Consultation de la consommation alimentaire des animaux


Identifiants de connexion :

•	Administrateur :
Username : admin@arcadia.com
Mot de passe : for.ls4szS22

•	Vétérinaire :
Username : veterinairemarielle@gmail.com
Mot de passe : veto5864-Ayt

•	Employé :
Username : employegilbert@gmail.com
Mot de passe : empz-517zTs

Technologies utilisées

•	Laravel:
Structure claire et code facile à gérer.
Automatisation des tâches répétitives.
Gestion fluide des bases de données.
Sécurisation aisée des applications.

•	HTML:
Structure fondamentale des pages web.
Affichage du contenu textuel.
Navigation entre les pages.

•	CSS:
Mise en forme visuelle des pages web.
Adaptation aux différents écrans.
Interface utilisateur harmonieuse.

•	Bootstrap:
Éléments d'interface prêts à l'emploi.
Mise en page flexible et adaptative.
Styles de base pour différents types d'applications.
Développement plus rapide et plus facile.

•	PHP:
Traitement des requêtes web.
Manipulation des bases de données.
Implémentation des fonctionnalités de l'application.

•	JavaScript:
Interactivité des pages web.
Amélioration de l'expérience utilisateur.

•	phpMyAdmin:
Gestion des bases de données MySQL.
Exécution de requêtes SQL.
Importation et exportation de données.

A savoir : le site est hébergé via un cPanel. La base de donnée complète se trouve sur le site web.