<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNourrituresTable extends Migration
{
    public function up()
    {
        Schema::create('nourritures', function (Blueprint $table) {
            $table->id();
            $table->date('date');
            $table->time('heure');
            $table->foreignId('animal_id')->constrained()->onDelete('cascade');
            $table->string('detail');
            $table->string('quantité');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('nourritures');
    }
}