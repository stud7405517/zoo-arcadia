<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Habitat;

class HabitatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Habitat::factory()->create([
            'name'=> 'Savane',
            'image_id' => 1,
        ]);
        Habitat::factory()->create([
            'name'=> 'Prairie',
            'image_id' => 2,
        ]);
    }
}
