<?php

namespace Database\Seeders;

use App\Models\User;
// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // User::factory(10)->create();

        $this->call(RoleSeeder::class,);

        User::factory()->create([
            'firstname' => 'Admin',
            'lastname' => 'Arcadia',
            'email' => 'admin@arcadia.com',
            'role_id' => 3,
        ]);

        $this->call([
            RoleSeeder::class,
            ImageSeeder::class,
            HabitatSeeder::class,
            RaceSeeder::class,
            AnimalSeeder::class,
            VeterinaryReportSeeder::class,
            OpinionSeeder::class,
            ServiceSeeder::class,
            NourritureSeeder::class,
        ]);

    }
}
