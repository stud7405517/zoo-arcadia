<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Image;

class ImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $images = [
            [
                'path' => '/img/zebre.jpeg',
            ],
            [
                'path' => '/img/rhinoceros.jpeg',
            ],
        ];

        foreach ($images as $image) {
            Image::firstOrCreate([
                'path' => $image['path'],
            ]);
        }
    }
}
