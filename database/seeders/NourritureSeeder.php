<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Nourriture;

class NourritureSeeder extends Seeder
{
    public function run(): void
    {
        Nourriture::factory(5)->create();
    }
}