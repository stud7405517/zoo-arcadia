<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\VeterinaryReport;
use Illuminate\Database\Seeder;

class VeterinaryReportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        VeterinaryReport::factory(5)->create();
    }
}
