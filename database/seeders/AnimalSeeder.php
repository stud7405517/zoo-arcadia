<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Animal;
use Illuminate\Database\Seeder;

class AnimalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Animal::factory()->create([
            'habitat_id' => 1,
        ]);
        Animal::factory()->create([
            'habitat_id' => 2,
        ]);
    }
}
