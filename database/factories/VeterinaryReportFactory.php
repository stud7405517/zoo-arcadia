<?php

namespace Database\Factories;
use App\Models\Animal;


use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\VeterinaryReport>
 */
class VeterinaryReportFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'date' => fake()->date(),
            'detail' => fake()->sentence(),
            'animal_id'=> rand(1, Animal::all()->count()),
        ];
    }
}
