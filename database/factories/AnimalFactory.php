<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Habitat;
use App\Models\Race;


/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Animal>
 */
class AnimalFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'firstname' => fake()->firstname(),
            'status' => fake()->word(),
            'scoreView' => 0,
            'habitat_id'=> rand(1, Habitat::all()->count()),
            'race_id'=> rand(1, Race::all()->count()),
        ];
    }
}
