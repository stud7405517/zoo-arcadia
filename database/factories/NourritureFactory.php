<?php

namespace Database\Factories;

use App\Models\Nourriture;
use App\Models\Animal;
use Illuminate\Database\Eloquent\Factories\Factory;

class NourritureFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */

    public function definition(): array
    {
        return [
            'date' => fake()->date(),
            'heure' => fake()->time($format = 'H:i'),
            'animal_id' => Animal::factory(),
            'detail' => fake()->sentence(),
            'quantité' => fake()->sentence(), // Assuming quantity is a float
        ];
    }
}